//
//  Queue.h
//  queue
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef Queue_h
#define Queue_h
#include "QueueElement.h"
typedef struct Queue Queue;
struct Queue {
    QueueElement *firstElement;
    int length;
};

#endif /* Queue_h */
