//
//  main.c
//  queue
//
//  Created by Tatadmound on 22/04/2021.
//

#include <stdio.h>
#include "queueManager.h"

int main(int argc, const char * argv[]) {
    Queue *queue = initQueue();
    
    queueing(queue, 4);
    queueing(queue, 8);
    queueing(queue, 15);
    queueing(queue, 16);
    queueing(queue, 23);
    queueing(queue, 42);
    
    printf("State of the queue :\n");
    showQueue(queue);
    
    printf("I unqueue %d\n", unqueueing(queue));
    printf("I unqueue %d\n", unqueueing(queue));
    
    
    printf("State of the queue :\n");
    showQueue(queue);
    return 0;
}
