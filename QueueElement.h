//
//  QueueElement.h
//  queue
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef QueueElement_h
#define QueueElement_h
typedef struct QueueElement QueueElement;
struct QueueElement {
    int value;
    QueueElement *nextElement;
};

#endif /* QueueElement_h */
