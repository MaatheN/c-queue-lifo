//
//  queueManager.c
//  queue
//
//  Created by Tatadmound on 22/04/2021.
//

#include "queueManager.h"

Queue* initQueue(){
    Queue *queue = malloc(sizeof(Queue));
    queue->firstElement=NULL;
    queue->length=0;
    
    return queue;
}

char queueing(Queue *queue, int value){
    QueueElement *newElement = malloc(sizeof(QueueElement));
    if (queue==NULL || newElement == NULL) {
        exit(EXIT_FAILURE);
    }
    newElement->value=value;
    newElement->nextElement=NULL;
    //if the queue is empty
    if (queue->length <= 0) {
        queue->firstElement=newElement;
    }else {
        QueueElement *lastQueueElement = queue->firstElement;
        //go to the end of the queue
        while(lastQueueElement->nextElement!=NULL){
            lastQueueElement=lastQueueElement->nextElement;
        }
        lastQueueElement->nextElement=newElement;
    }
    queue->length++;
    return 0;
}

int unqueueing(Queue *queue){
    if (queue == NULL) {
        exit(EXIT_FAILURE);
    }
    int deletedValue = 0;
    //something to unqueue ?
    if (queue->firstElement != NULL) {
        QueueElement *elementToDelete = queue->firstElement;
        //value to return
        deletedValue=elementToDelete->value;
        queue->firstElement=elementToDelete->nextElement;
        free(elementToDelete);
    }
    queue->length--;
    return deletedValue;
}

char showQueue(Queue *queue){
    //1) show the first element
    QueueElement *currentQueueElementPointer = queue->firstElement;
    while (currentQueueElementPointer!=NULL) {
        printf("%d ", currentQueueElementPointer->value);
        currentQueueElementPointer=currentQueueElementPointer->nextElement;
    }
    printf("\n");
    //2) go to next, if it exist 3) else 4)
    //3) show his value and 2)
    //4) say end of queue
    return 0;
    
}
