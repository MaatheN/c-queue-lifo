//
//  queueManager.h
//  queue
//
//  Created by Tatadmound on 22/04/2021.
//

#ifndef queueManager_h
#define queueManager_h

#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"
//#include "QueueElement.h"

Queue* initQueue(void);
char queueing(Queue *queue, int value);
int unqueueing(Queue *queue);
char showQueue(Queue *queue);
#endif /* queueManager_h */
